<?php
  session_start();
  //if(isset($_SESSION['cpf'])) {
  if(isset($_SESSION['nome'])) {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
    <title>ImoWEB</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="icon" href="img/casa.png" type="image/png" sizes="">
  <!-- <link href="css/bootstrap.css" rel='stylesheet' type='text/css' /> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">  -->
  <!-- <link rel="stylesheet" href="css/styles.css"> -->

  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootbox.min.js"></script>
  <script src="js/bootbox.js"></script> 
</head>
<body>
  <body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="#">IMOVEISWEB</a>
      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <!-- <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2"> -->
          <div class="input-group-append"></div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">2+</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
            <a class="dropdown-item" href="#">Notificações</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <span class="badge badge-danger">1</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
            <a class="dropdown-item" href="#">Check Email</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>
    </nav>

    <div id="wrapper">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active"></li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Settings:</h6>
            <a class="dropdown-item" href="forgot-password.html">Mudar Password</a>
          </div>
        </li>
      </ul>

      <div id="content-wrapper">
        <div class="container-fluid">

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
                IMOVEIS</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Proprietário</th>
                      <th>Local</th>
                      <th>Valor</th>
                      <th>Descrição</th>
                      <th>Confirm</th> 
                    </tr>
                  </thead>

                  <?php 
                    include 'conexao.php';

                    $pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
                    $cmd = "select * from imoveis";
                    $imoveis = mysqli_query($link, $cmd);
                    $total = mysqli_num_rows($imoveis); 
                    $registros = 10;
                    $numPaginas = ceil($total/$registros);
                    $inicio = ($registros*$pagina)-$registros;

                    if(isset($_REQUEST['filtro']))
                      $filtro = $_REQUEST['filtro'];
                    else
                      $filtro ="";
                    #$sql = "SELECT usuid, usunome, usuemail FROM sa_usuario WHERE usuarionome LIKE '$filtro%' ORDER BY usunome limit $inicio,$registros " ;
                    $sql = "SELECT * FROM imoveis  
                            INNER JOIN fiador ON imoveis.codigo_fiador = fiador.codigo_fiador
                            WHERE id LIKE '%$filtro%' ORDER BY id limit $inicio,$registros";
                      $resultado    = mysqli_query($link, $sql);
                      $num          = mysqli_num_rows($resultado);
                      while($registro = mysqli_fetch_array($resultado)) {
                      $id           = $registro['nome_fiador'];
                      $cidade       = $registro['cidade'];
                      $valor        = $registro['valor'];
                      $descricao    = $registro['descricao'];
                      
                      
                      echo '<tr>';
                      echo '<td align="center">'. $id .'</td>';
                      echo '<td>'. $cidade .'</td>';
                      echo '<td>'. $valor .'</td>';
                      echo '<td> <a href="" data-toggle="modal" data-target="#imovel"/>Consultar Anuncio</a></td>';
                      echo '<td><a class="btn btn-default" href="contrato.php?id='.$id.'">Alugar <span class="glyphicon glyphicon-wrench" /></a></td>';
                      echo '&nbsp;';
                      ?>

                      <?php 
                      //echo '<a class="btn btn-danger" href="form-delete-usuario.php?usuid='.$usuid.'">Delete <span class="glyphicon glyphicon-trash" /></a>';
                      echo '</td>';
                      echo '</tr>';

                    }
                    
                  ?>
                  
  
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 20:05 PM</div>
          </div>

        </div>

        <!-- /.container-fluid -->
        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © IMOVEIS WEB 2019</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Deseja Sair?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Selecione "Logout" se você estiver pronto para encerrar sua sessão.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- #modal descriçao imovel -->
    <div class="modal fade" id="imovel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Descrição do Imovel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="card" style="width:420px">
                <div class="card-body">
                  <p class="card-text"><?php echo '<td> <a href="" data-toggle="modal" data-target="#imovel"/></a>'. $descricao .'</td>';?> </p>
                </div>
              </div>
              <br>
          </div>

        </div>
      </div>
    </div>
          
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>
    
    

</body> 
</html>  
<?php
} else
      header('location:index.php');
?>